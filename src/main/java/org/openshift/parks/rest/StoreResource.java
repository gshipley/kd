package org.openshift.parks.rest;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.openshift.parks.domain.Coordinates;
import org.openshift.parks.domain.Store;
import org.openshift.parks.mongo.DBConnection;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import java.util.ArrayList;
import java.util.List;

@RequestScoped
@Path("/stores")
public class StoreResource {

	@Inject
	private DBConnection dbConnection;

	private Store populateStoreInformation(Document dataValue) {
		Store newObject = new Store();
		newObject.setId((ObjectId) dataValue.get("_id"));
		newObject.setName(dataValue.get("Address"));
		Coordinates cord = new Coordinates((List) dataValue.get("pos"));
		newObject.setPosition(cord);
		newObject.setLatitude(cord.getLatitude());
		newObject.setLongitude(cord.getLongitude());
		newObject.setPhoneNumber(dataValue.get("Phone Number"));


		return newObject;
	}

	// get all the mlb parks
	@GET()
	@Produces("application/json")
	public List<Store> getAllStores() {
		ArrayList<Store> allStoreList = new ArrayList<Store>();
		System.out.println("Hi..");
		MongoCollection stores = dbConnection.getCollection();
		MongoCursor<Document> cursor = stores.find().iterator();
		try {
			while (cursor.hasNext()) {
				allStoreList.add(this.populateStoreInformation(cursor.next()));
			}
		} finally {
			cursor.close();
		}

		return allStoreList;
	}

	@GET
	@Produces("application/json")
	@Path("within")
	public List<Store> findStoresWithin(@QueryParam("lat1") float lat1,
										@QueryParam("lon1") float lon1, @QueryParam("lat2") float lat2,
										@QueryParam("lon2") float lon2) {

	    // The first thing we want to do is make sure the DB contains data
        dbConnection.checkDatabase();
		ArrayList<Store> allStoreList = new ArrayList<Store>();
		MongoCollection stores = dbConnection.getCollection();

		// make the query object
		BasicDBObject spatialQuery = new BasicDBObject();

		ArrayList<double[]> boxList = new ArrayList<double[]>();
		boxList.add(new double[] { new Float(lon2), new Float(lat2) });
		boxList.add(new double[] { new Float(lon1), new Float(lat1) });

		BasicDBObject boxQuery = new BasicDBObject();
		boxQuery.put("$box", boxList);

		spatialQuery.put("pos", new BasicDBObject("$within", boxQuery));
		System.out.println("Using spatial query: " + spatialQuery.toString());

		MongoCursor<Document> cursor = stores.find(spatialQuery).iterator();
		try {
			while (cursor.hasNext()) {
				allStoreList.add(this.populateStoreInformation(cursor.next()));
			}
		} finally {
			cursor.close();
		}
		System.out.println("Return " + allStoreList.size() + " stores");

		return allStoreList;
	}
}
